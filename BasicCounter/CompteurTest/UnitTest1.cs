﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterC;

namespace CompteurTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Testgetvaleur()
        {
            Assert.AreEqual(Compteur.getvaleur(),0);
        }        

        [TestMethod]
        public void Testincremvaleur()
        {
            Compteur.incremvaleur();
            Assert.AreEqual(Compteur.getvaleur(), 1);
        }
        [TestMethod]
        public void Testdecremvaleur()
        {

            Compteur.decremvaleur();
            Assert.AreEqual(Compteur.getvaleur(), 0);
        }
        [TestMethod]
        public void Testreset()
        {
            Compteur.decremvaleur();
            Compteur.resetvaleur();
            Assert.AreEqual(Compteur.getvaleur(), 0);
        }


    }
}
