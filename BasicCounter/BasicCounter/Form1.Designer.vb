﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.increm = New System.Windows.Forms.Button()
        Me.decrem = New System.Windows.Forms.Button()
        Me.reset = New System.Windows.Forms.Button()
        Me.valeur = New System.Windows.Forms.Label()
        Me.Lbl1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'increm
        '
        Me.increm.Location = New System.Drawing.Point(259, 95)
        Me.increm.Name = "increm"
        Me.increm.Size = New System.Drawing.Size(115, 25)
        Me.increm.TabIndex = 0
        Me.increm.Text = "+"
        Me.increm.UseVisualStyleBackColor = True
        '
        'decrem
        '
        Me.decrem.Location = New System.Drawing.Point(33, 95)
        Me.decrem.Name = "decrem"
        Me.decrem.Size = New System.Drawing.Size(100, 25)
        Me.decrem.TabIndex = 1
        Me.decrem.Text = "-"
        Me.decrem.UseVisualStyleBackColor = True
        '
        'reset
        '
        Me.reset.Location = New System.Drawing.Point(137, 153)
        Me.reset.Name = "reset"
        Me.reset.Size = New System.Drawing.Size(123, 25)
        Me.reset.TabIndex = 2
        Me.reset.Text = "RAZ"
        Me.reset.UseVisualStyleBackColor = True
        '
        'valeur
        '
        Me.valeur.AutoSize = True
        Me.valeur.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.valeur.Location = New System.Drawing.Point(173, 95)
        Me.valeur.Name = "valeur"
        Me.valeur.Size = New System.Drawing.Size(26, 29)
        Me.valeur.TabIndex = 3
        Me.valeur.Text = "0"
        '
        'Lbl1
        '
        Me.Lbl1.AutoSize = True
        Me.Lbl1.Location = New System.Drawing.Point(175, 55)
        Me.Lbl1.Name = "Lbl1"
        Me.Lbl1.Size = New System.Drawing.Size(40, 17)
        Me.Lbl1.TabIndex = 4
        Me.Lbl1.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(388, 246)
        Me.Controls.Add(Me.Lbl1)
        Me.Controls.Add(Me.valeur)
        Me.Controls.Add(Me.reset)
        Me.Controls.Add(Me.decrem)
        Me.Controls.Add(Me.increm)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents increm As Button
    Friend WithEvents decrem As Button
    Friend WithEvents reset As Button
    Friend WithEvents valeur As Label
    Friend WithEvents Lbl1 As Label
End Class
