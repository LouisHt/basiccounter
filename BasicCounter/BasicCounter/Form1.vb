﻿Imports BasicCounterC

Public Class Form1

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles decrem.Click
        BasicCounterC.Compteur.decremvaleur()
        valeur.Text = BasicCounterC.Compteur.getvaleur
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles increm.Click
        BasicCounterC.Compteur.incremvaleur()
        valeur.Text = BasicCounterC.Compteur.getvaleur
    End Sub

    Private Sub reset_Click(sender As Object, e As EventArgs) Handles reset.Click
        BasicCounterC.Compteur.resetvaleur()
        valeur.Text = BasicCounterC.Compteur.getvaleur
    End Sub

End Class
