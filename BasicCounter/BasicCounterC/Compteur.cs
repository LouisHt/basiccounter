﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterC
{
    public class Compteur
    {
        private static int valeur;

        public static int getvaleur()
        {
            return valeur;

        }

        public static void incremvaleur()
        {
            valeur++;
        }

        public static void decremvaleur()
        {
            valeur--;
        }

        public static void resetvaleur()
        {
            valeur = 0;
        }
    }
}
